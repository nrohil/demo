package com.app.service;

import com.app.exception.CustomException;

public interface IReadProtocolService {

	String readFile(String filePath) throws CustomException;
}
