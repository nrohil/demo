package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.app.exception.CustomException;

	public class HTTPService implements IReadProtocolService {
		

		@Override
		public String readFile(String filePath) throws CustomException {

			System.out.println("Connecting to FTP server...");
			URL url;
			String directoryPath = "D:\\files\\"; // get from configuration
			
			String filePathToWriteTo = directoryPath + getFileNameFromPath(filePath);
			File fileToWriteTo = new File(filePathToWriteTo); 
			
			FileWriter fr = null;
			BufferedWriter br = null;
			try {
				url = new URL(filePath);
				URLConnection con = url.openConnection();

				fr = new FileWriter(filePathToWriteTo + (int) (Math.random() * 1000) + ".txt");
				br = new BufferedWriter(fr);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				System.out.println("Reading file start.");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					writeUsingBufferedWriter(fileToWriteTo, fr, br, inputLine);	// write in the intermediate hard file
				}
				in.close();
			} catch (FileNotFoundException e) {
				System.out.println("File not find on server.");
				System.exit(0);
			} catch (Exception e) {
				throw new CustomException("Error in file reading writing.", e);
			} finally {
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Read File Complete.");

			return "Success";
		}


		private String getFileNameFromPath(String filePath) {
			int indexOfFile = 0;
			indexOfFile = new File(filePath).getAbsolutePath().lastIndexOf("/");
			if (indexOfFile == -1) {
				indexOfFile = new File(filePath).getAbsolutePath().lastIndexOf("\\");
			}
			System.out.println("done");
			String filename = new File(filePath).getAbsolutePath().split("\\")[1];

			return filename;
		}

		private void writeUsingBufferedWriter(File file, FileWriter fr, BufferedWriter br, String inputLine) {
			try {
				br.write(inputLine);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

}
