package com.app.service;

import com.app.exception.CustomException;

public interface IReaderService  {
	public void loadFilesData() throws CustomException ;

}