package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import com.app.exception.CustomException;

public class ReaderService implements IReaderService {

	private IReadProtocolService iReadProtocolService;

	private List<String> listOfFilePaths = new ArrayList<>();
	private Map<String, List<String>> protocolFilePathMap = new HashMap<>();
	private Map<String, IReadProtocolService> protocolStrategiesMap = new HashMap<>();

	static final String consolidatedPathsFilePath = "D:\\readFolder\\FilePaths.txt";

	public void fillMapWithAllProtocols() {
		protocolFilePathMap.put("FTP", null);
		protocolFilePathMap.put("HTTP", null);
		protocolFilePathMap.put("ABC", null);
	}

	public void fillMapWithStrategies() {
		protocolStrategiesMap.put("FTP", new FTPService());
		protocolStrategiesMap.put("HTTP", new HTTPService());
		protocolStrategiesMap.put("ABC", null);
	}

	public void loadFilesData() throws CustomException {
		System.out.println("......here....");

		fillMapWithAllProtocols();
		fillMapWithStrategies();

		List<String> newFilesToRead = null;
		try {

			if (this.consolidatedPathsFilePath == null || this.consolidatedPathsFilePath.isEmpty()) {
				throw new CustomException("property value missing in .props file!");
			}

			readNewFilePathsFromConsolidated(consolidatedPathsFilePath, false);
			// validate(); // to validate the data inside the file
		} catch (Exception e) {
			throw new CustomException("", e);
		}

		ExecutorService eService = Executors.newFixedThreadPool(2);
		List<Task> callables = new ArrayList<>();

		for (Iterator itr = protocolFilePathMap.entrySet().iterator(); itr.hasNext();) {
			Entry entry = (Entry) itr.next();
			String protocol = (String) entry.getKey();
			List<String> paths = (List<String>) entry.getValue();

			if (paths != null) {
				for (String path : paths) {
					Task a = new Task(path, protocol, protocolStrategiesMap.get(protocol));
					callables.add(a);
				}
			}
		}
		try {
			List<Future<String>> futures = eService.invokeAll(callables);

			Thread.sleep(5000);
			for (Future<String> future : futures) {

				String dat;
				dat = future.get();
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		eService.shutdown();
	}

	private void readNewFilePathsFromConsolidated(String consolidatedFilePaths, boolean readAll)
			throws CustomException {

		List<String> freshFilePaths = new ArrayList<String>();

		try (BufferedReader bReader = Files.newBufferedReader(Paths.get(consolidatedFilePaths))) {
			freshFilePaths = bReader.lines().collect(Collectors.toList());
			if (freshFilePaths.isEmpty()) {
				throw new CustomException("file paths not there in consolidated files !");
			}
			for (int i = 0; i < freshFilePaths.size(); i++) {
				String row = freshFilePaths.get(i);
				String protocol = (row.split("="))[0].toUpperCase();
				List<String> l = null;
				if (protocolFilePathMap.get(protocol) == null) {
					l = new ArrayList<>();
					l.add(row.split("=")[1]);
					protocolFilePathMap.put(protocol, l);
				} else {
					l = protocolFilePathMap.get(protocol);
					l.add(row.split("=")[1]);
				}
			}
		} catch (Exception e) {
			throw new CustomException("Error in file reading.", e);
		}
	}

	public List<String> getListOfFilePaths() {
		return listOfFilePaths;
	}

	static class Task implements Callable<String> {
		String path;
		private String protocolType;
		private IReadProtocolService iReadProtocolService;

		public Task(String path, String protocolType, IReadProtocolService iReadProtocolService) {
			this.path = path;
			this.protocolType = protocolType;
			this.iReadProtocolService = iReadProtocolService;
		}

		private String readAllFilesData(String filePath) throws CustomException {

			System.out.println("Connecting to FTP server...");
			URL url;
			String directoryPath = "D:\\files\\"; // get from configuration

			String filePathToWriteTo = directoryPath + getFileNameFromPath(filePath);
			File fileToWriteTo = new File(filePathToWriteTo);

			FileWriter fr = null;
			BufferedWriter br = null;
			try {
				url = new URL(filePath);
				URLConnection con = url.openConnection();

				fr = new FileWriter(filePathToWriteTo + (int) (Math.random() * 1000) + ".txt");
				br = new BufferedWriter(fr);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				System.out.println("Reading file start.");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					writeUsingBufferedWriter(fileToWriteTo, fr, br,
							inputLine); /* write in intermediate hard file */

				}
				in.close();
			} catch (FileNotFoundException e) {
				System.out.println("File not find on server.");
				System.exit(0);
			} catch (Exception e) {
				throw new CustomException("Error in file reading writing.", e);
			} finally {
				try {
					br.close();
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Read File Complete.");

			return "Success";
		}

		private String getFileNameFromPath(String filePath) {
			int indexOfFile = 0;
			indexOfFile = new File(filePath).getAbsolutePath().lastIndexOf("/");
			if (indexOfFile == -1) {
				indexOfFile = new File(filePath).getAbsolutePath().lastIndexOf("\\");
			}
			System.out.println("done");
			String filename = new File(filePath).getAbsolutePath().split("\\")[1];

			return filename;
		}

		private void writeUsingBufferedWriter(File file, FileWriter fr, BufferedWriter br, String inputLine) {
			try {
				br.write(inputLine);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public String call() throws Exception {
			try {

				this.iReadProtocolService.readFile(path);
			} catch (CustomException e) {
				e.printStackTrace();
			}
			return "";
		}

	}

}