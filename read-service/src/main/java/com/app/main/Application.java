package com.app.main;

public class Application {

	public static void main(String[] args) {
		try {
			  new Controller().start();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
