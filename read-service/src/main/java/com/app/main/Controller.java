package com.app.main;

import com.app.exception.CustomException;
import com.app.service.IReaderService;
import com.app.service.ReaderService;

public class Controller {

	private static boolean writeDataFromFiles() throws CustomException {

		try {
			IReaderService readerS = getReaderService();
			readerS.loadFilesData();

		} catch (Exception e) {
			throw new CustomException("error in saving data !", e);
		}

		return false;
	}

	private static IReaderService getReaderService() {

		return new ReaderService();
	}

	public boolean start() {
		try {
			writeDataFromFiles();
		} catch (CustomException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return false;
	}

}
